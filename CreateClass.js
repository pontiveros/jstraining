
function ClassA (msg) {
    this.message = msg;
    
    function showMessage() {
        return message;
    }
}

function entryPoint() {
    var a = new ClassA("Hello JavaScript");
    // document.writeln(a.message);
    // document.writeln(a.showMessage());   
}

entryPoint();