/**
 * Node class to build a tree.
 * 
 **/

function TNode (value) {
    this.value = value;
    this.left  = null;
    this.right = null;
    
    this.add = function (param) {
        if (this.value >= param.value) {
            if (this.left == null)
                this.left = param;
            else
                this.left.add(param);
        } else {
           if (this.right == null)
                this.right = param;
            else
                this.right.add(param);
        }
    }
    
    // This function needs an array as param.
    this.getPreOrder = function(items) {
        if (this.left != null)
            this.left.getPreOrder(items);
            
        items.push(this.value);
        
        if (this.right != null)
            this.right.getPreOrder(items);
    }
}

/**
 * TEntity class
 *
 **/

function TEntity (instance, identification, fullname) {
    // Properties
    this.instance       = instance;
    this.identification = identification;
    this.fullname       = fullname;
    
    // Methods
    this.isEqualTo = function (param) {
        if (this.fullname > param.fullname)
            return 1;
        else if (this.fullname < param.fullname)
            return -1;
        else 
            return 0;
    }
}

// This function needs an array as param.
function fnSort(items) {
    
    for (var x = 0; x < (items.length - 1); x++) {
        var m = x;
        for (var y = (x + 1); y < items.length; y++) {
            if (items[m].isEqualTo(items[y]) > 0) {
                m = y;
            }
        }
        
        if (m != x) {
            var  aux = items[x];
            items[x] = items[m];
            items[m] = aux;
        }
    }
}


